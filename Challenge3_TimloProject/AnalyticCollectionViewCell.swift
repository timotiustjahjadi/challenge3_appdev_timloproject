//
//  AnalyticCollectionViewCell.swift
//  Challenge3_TimloProject
//
//  Created by Timotius Tjahjadi  on 17/06/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class AnalyticCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var BarView: UIView!
    @IBOutlet weak var BarScoreLabel: UILabel!
    @IBOutlet weak var BarWeekLabel: UILabel!
    
    @IBOutlet weak var BarHeight: NSLayoutConstraint!
}
