//
//  Animal.swift
//  BatteryApp
//
//  Created by Timotius Tjahjadi  on 27/05/20.
//  Copyright © 2020 Learn. All rights reserved.
//

import UIKit

class Animal{
    var name = ""
    var rarity = ""
    var animalImage = ""
    var unlocked: Bool
    
    init(name: String, rarity: String, animalImage: String, unlocked: Bool) {
        self.name = name
        self.rarity = rarity
        self.animalImage = animalImage
        self.unlocked = unlocked
    }
    
    static func fetchAnimals() -> [Animal]{
        return [
            Animal(name: "Giraffe", rarity: "Common", animalImage: "GIRAFFE", unlocked: UserDefaultData.checkAnimalProgress(input: 0)),
            Animal(name: "Chicken", rarity: "Common", animalImage: "CHICKEN", unlocked: UserDefaultData.checkAnimalProgress(input: 1)),
            Animal(name: "Elephant", rarity: "Common", animalImage: "GAJAH", unlocked: UserDefaultData.checkAnimalProgress(input: 2)),
            Animal(name: "Crab", rarity: "Common", animalImage: "CRAB", unlocked: UserDefaultData.checkAnimalProgress(input: 3)),
            Animal(name: "Sloth", rarity: "Rare", animalImage: "SLOTH", unlocked: UserDefaultData.checkAnimalProgress(input: 4)),
            Animal(name: "Polar Bear", rarity: "Common", animalImage: "POLAR", unlocked: UserDefaultData.checkAnimalProgress(input: 5)),
            Animal(name: "T-Rex", rarity: "Super Rare", animalImage: "TREX", unlocked: UserDefaultData.checkAnimalProgress(input: 6)),
            Animal(name: "Ostrich", rarity: "Rare", animalImage: "OSTRICH", unlocked: UserDefaultData.checkAnimalProgress(input: 7)),
            Animal(name: "Koala", rarity: "Rare", animalImage: "KOALA", unlocked: UserDefaultData.checkAnimalProgress(input: 8)),
            Animal(name: "Peacock", rarity: "Super Rare", animalImage: "PEACOCK", unlocked: UserDefaultData.checkAnimalProgress(input: 9)),
            Animal(name: "Dodo", rarity: "Rare", animalImage: "DODO", unlocked: UserDefaultData.checkAnimalProgress(input: 10))
        ]
    }
    
}


