//
//  ItemCollectionViewCell.swift
//  Challenge3_TimloProject
//
//  Created by Timotius Tjahjadi  on 29/05/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ItemImageView: UIImageView!
    @IBOutlet weak var BoxItemImageView: UIImageView!
    
    var item: Items!
    var egg: Eggs!
    var activated: Bool = true{
        didSet{
            self.updateUI()
        }
    }
    
    func updateUI(){
        if activated{
            ItemImageView.image = UIImage(named: item.itemImage)
            
            //MARK: Change the item Color
            if item.unlocked == false {
                let templateImage =  ItemImageView.image?.withRenderingMode(.alwaysTemplate)
                ItemImageView.image = templateImage
                ItemImageView.tintColor = UIColor.black
            }
            //
            
        }
        else{
            ItemImageView.image = UIImage(named: egg.eggImage)
            
            //MARK: Change the egg Color
            if egg.unlocked == false {
                let templateImage =  ItemImageView.image?.withRenderingMode(.alwaysTemplate)
                ItemImageView.image = templateImage
                ItemImageView.tintColor = UIColor.black
            }
            //
        }
        
    }
}
