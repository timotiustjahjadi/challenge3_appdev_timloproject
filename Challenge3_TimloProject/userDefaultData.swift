//
//  userDefaultData.swift
//  Challenge3_TimloProject
//
//  Created by Timotius Tjahjadi  on 01/06/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

let defaults = UserDefaults.standard

struct UserDefaultData {
    
    static func saveSelectedAnimal(selectedAnimal: Int){
        defaults.set(selectedAnimal, forKey: "animalKey")
    }
    
    static func checkSelectedAnimal() -> Int{
        let selectedAnimalUD = defaults.integer(forKey: "animalKey")
        return selectedAnimalUD
    }
    
    static func saveSelectedAItem(selectedItem: Int){
        defaults.set(selectedItem, forKey: "itemKey")
    }
    
    static func checkSelectedItem() -> Int{
        let selectedItemUD = defaults.integer(forKey: "itemKey")
        return selectedItemUD
    }
    
    static func saveSelectedEgg(selectedEgg: Int){
        defaults.set(selectedEgg, forKey: "eggKey")
    }
    
    static func checkSelectedEgg() -> Int{
        let selectedEggUD = defaults.integer(forKey: "eggKey")
        return selectedEggUD
    }
    
    static func saveAnimalProgress(animalUnlockedList: [String]){
        defaults.set(animalUnlockedList, forKey: "animalUnlocked")
    }
    
    static func checkAnimalProgress(input: Int) -> Bool{
        let animalUnlockedUD = defaults.stringArray(forKey: "animalUnlocked")
        
        if animalUnlockedUD?[input] == "T" {
            return true
        }
        else{
            return false
        }
    }
    
    static func saveItemProgress(itemUnlockedList: [String]){
        defaults.set(itemUnlockedList, forKey: "itemUnlocked")
    }
    
    static func checkItemProgress(input: Int) -> Bool{
        let itemUnlockedUD = defaults.stringArray(forKey: "itemUnlocked")
        
        if itemUnlockedUD?[input] == "T" {
            return true
        }
        else{
            return false
        }
    }
    static func saveAnimalGet(animalInput: String){
        var animalUnlockedUD = defaults.stringArray(forKey: "animalUnlocked")
        let arrayOfAnimal = Animal.fetchAnimals()
        var index: Int = 0
        for animalss in arrayOfAnimal {
            if animalss.animalImage == animalInput {
                animalUnlockedUD?.remove(at: index)
                animalUnlockedUD?.insert("T", at: index)
            }
            index += 1
        }
        
        defaults.set(animalUnlockedUD, forKey: "animalUnlocked")
        index = 0
    }
    
    static func saveItemGet(Input: Int){
        var itemUnlockedUD = defaults.stringArray(forKey: "itemUnlocked")
        
        itemUnlockedUD?.remove(at: Input)
        itemUnlockedUD?.insert("T", at: Input)
    
        defaults.set(itemUnlockedUD, forKey: "itemUnlocked")
    }
    
    static func saveEggProgress(eggUnlockedList: [String]){
        defaults.set(eggUnlockedList, forKey: "eggUnlocked")
    }
    
    static func checkEggProgress(input: Int) -> Bool{
        let eggUnlockedUD = defaults.stringArray(forKey: "eggUnlocked")
        
        if eggUnlockedUD?[input] == "T" {
            return true
        }
        else{
            return false
        }
    }
    static func checkChargingProgress() -> Int{
        let chargingUD = defaults.integer(forKey: "chargingSaved")
        return chargingUD
    }
    
    static func saveChargingProgress(){
        var check = checkChargingProgress()
        check+=1
        defaults.set(check, forKey: "chargingSaved")
    }
    
    static func resetChargingProgress(){
        defaults.set(0, forKey: "chargingSaved")
    }
    
    static func isAppAlreadyLaunchedOnce() -> Bool{
        if let isAppAlreadyLaunchedOnce = defaults.string(forKey: "isAppAlreadyLaunchedOnce"){
            // print("App already launched : \(isAppAlreadyLaunchedOnce)")
            return true
        }else{
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
            // print("App launched first time")
            return false
        }
    }
    
    static func saveRecordSucc(Succ: Int){
        defaults.set(Succ, forKey: "SuccessRecord")
        
    }
    static func saveRecordFail(Fail: Int){
        defaults.set(Fail, forKey: "FailRecord")
    }
    
    static func hasStart() {
        defaults.set(true, forKey: "hasStartWeekly")
    }
    
    static func dailyDate(date: String) {
        defaults.set(date, forKey: "LastDate")
    }
    
    static func DayCount(Day: Int, Reset: Bool) {
        defaults.set(Day, forKey: "DayTotal")
        if Reset == false {
            defaults.set(Day, forKey: "DayPassed")
        }
        else {
            defaults.set(0, forKey: "DayPassed")
        }
    }
    
    static func saveWeeklyRecord(WSucc: Int, WFail: Int) {
        defaults.set(WSucc, forKey: "TempSucc")
        defaults.set(WFail, forKey: "TempFail")
        var ArraySuccess = defaults.array(forKey: "arrSuccess")
        ArraySuccess?.append(WSucc)
        defaults.set(WSucc, forKey: "arrSuccess")
        var ArrayFail = defaults.array(forKey: "arrFail")
        ArrayFail?.append(WFail)
        defaults.set(WFail, forKey: "arrFail")
        
    }
    static func DataRecord(Srec: Int, Frec: Int, switcher: Bool) {
        if switcher ==  true{
            //just append
            var switchSuccess = defaults.array(forKey: "arrSuccess")
            switchSuccess?.append(Srec)
            defaults.set(switchSuccess, forKey: "arrSuccess")
            
            var switchFail = defaults.array(forKey: "arrFail")
            switchFail?.append(Frec)
            defaults.set(switchFail, forKey: "arrFail")
            
        }
        else if switcher == false{
            
            var ArraySuccess = defaults.array(forKey: "arrSuccess")
            if ArraySuccess == nil{
                ArraySuccess = [Srec]
                //ArraySuccess?.append(Srec)
                // print("RECORD SUCCESS FIRST")
            }
            else {
                // print("RECORD SUCCESS NOT FIRST")
                ArraySuccess?.removeLast()
                ArraySuccess?.append(Srec)
            }
            defaults.set(ArraySuccess, forKey: "arrSuccess")
            
            var ArrayFail = defaults.array(forKey: "arrFail")
            if ArrayFail == nil {
                ArrayFail = [Frec]
                //ArrayFail?.append(Frec)
                // print("RECORD FAIL FIRST")
            }
            else {
                ArrayFail?.removeLast()
                ArrayFail?.append(Frec)
                // print("RECORD FAIL NOT FIRST")
            }
            defaults.set(ArrayFail, forKey: "arrFail")
            // print("Recorded ***************")
        }
    }
    static func saveScore() {
        
    }
    
    static func getDataSuccess() -> [Int]{
        if defaults.array(forKey: "arrSuccess") == nil{
                return [0]
        }
        return defaults.array(forKey: "arrSuccess") as! [Int]
    }
    static func getDataFail() -> [Int]{
        if defaults.array(forKey: "arrFail") == nil {
            return [0]
        }
        return defaults.array(forKey: "arrFail") as! [Int]
    }
    
}

