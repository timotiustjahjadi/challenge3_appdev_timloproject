//
//  GameScene.swift
//  cobaSpriteKit
//
//  Created by aldo on 19/05/20.
//  Copyright © 2020 aldo. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    let background = SKSpriteNode(imageNamed: "Background")
    private var char = SKSpriteNode()
    private var egg = SKSpriteNode()
    private var item = SKSpriteNode()
    var animals = Animal.fetchAnimals()
    var items = Items.fetchItem()
    var eggs = Eggs.fetchEgg()
    private var WalkingChar1: [SKTexture] = []
    private var WalkingChar2: [SKTexture] = []
    weak var viewController: ViewController?
    weak var chargingViewController: ChargingViewController?
    override func didMove(to view: SKView) {
    buildChar()
    buildChar2()
    backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    background.position = CGPoint(x: size.width/2, y: size.height/2)
    addChild(background)
//        selectedAnimal = animals[UserDefaultData.checkSelectedAnimal()].name

    }
    func buildChar() {
//       print(selectedAnimal)
        
      let AnimatedAtlas1 = SKTextureAtlas(named: animals[UserDefaultData.checkSelectedAnimal()].name)
      var walkFrames: [SKTexture] = []

      let numImages = AnimatedAtlas1.textureNames.count
        for i in 0...numImages {
            let TextureName = "\(animals[UserDefaultData.checkSelectedAnimal()].animalImage)_\(i)"
        walkFrames.append(AnimatedAtlas1.textureNamed(TextureName))
      }
      WalkingChar1 = walkFrames
        let firstFrameTexture = WalkingChar1[0]
        char = SKSpriteNode(texture: firstFrameTexture)
        char.position = CGPoint(x: 230, y: 440)
        char.setScale(0.35)
        char.zPosition = 14
        addChild(char)
    }
    
func buildChar2() {
    egg = SKSpriteNode(imageNamed: eggs[UserDefaultData.checkSelectedEgg()].eggImage)
   egg.position = CGPoint(x: 140, y: 440)
    egg.setScale(0.8)
    egg.zPosition = 12
    
    item = SKSpriteNode(imageNamed: items[UserDefaultData.checkSelectedItem()].itemOnEggImage)
    item.position = CGPoint(x: 140, y: 440)
    item.setScale(0.8)
    item.zPosition = 13
   self.addChild(egg)
    self.addChild(item)
        }
    func animateChar() {
        char.isPaused = false
        char.run(SKAction.repeatForever(SKAction.sequence([SKAction.animate(with: WalkingChar1,
        timePerFrame: 0.02,
        resize: false,
        restore: true), SKAction.wait(forDuration: 10, withRange: 10)])),
        withKey:"walkingInPlace")
    }
    func charMoveEnded() {
    char.isPaused = true
    }
    func restartGame(){
//        let newScene = GameScene(size: self.size)
//        newScene.scaleMode = self.scaleMode
//        let animation = SKTransition.fade(withDuration: 1.0)
//        self.view?.presentScene(newScene, transition: animation)
        
        char.removeFromParent()
        egg.removeFromParent()
        item.removeFromParent()
    }
    
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//      let touch = touches.first!
//      let location = touch.location(in: self)
//      moveChar(location: location)
//    }
//
//    func moveChar(location: CGPoint){
//        var multiplierForDirection: CGFloat
//
//
//        let charSpeed = frame.size.width / 3.0
//
//
//        let moveDifference = CGPoint(x: location.x - char.position.x, y: location.y - char.position.y)
//        let distanceToMove = sqrt(moveDifference.x * moveDifference.x + moveDifference.y * moveDifference.y)
//
//
//        let moveDuration = distanceToMove / charSpeed
//
//
//        if moveDifference.x < 0 {
//          multiplierForDirection = -1.0
//        } else {
//          multiplierForDirection = 1.0
//        }
//        char.xScale = abs(char.xScale) * multiplierForDirection
//
//        // 1
//        if char.action(forKey: "walkingInPlace") == nil {
//          // if legs are not moving, start them
//          animateChar()
//        }
//
//        // 2
//        let moveAction = SKAction.move(to: location, duration:(TimeInterval(moveDuration)))
//
//        // 3
//        let doneAction = SKAction.run({ [weak self] in
//          self?.charMoveEnded()
//        })
//
//        // 4
//        let moveActionWithDone = SKAction.sequence([moveAction, doneAction])
//        char.run(moveActionWithDone, withKey:"charMoving")
//    }
}
