//
//  Animal.swift
//  BatteryApp
//
//  Created by Timotius Tjahjadi  on 27/05/20.
//  Copyright © 2020 Learn. All rights reserved.
//

import UIKit

class Eggs{
    var eggImage = ""
    var unlocked: Bool
    
    init(eggImage: String, unlocked: Bool){
        self.eggImage = eggImage
        self.unlocked = unlocked
    }
    
    static func fetchEgg() -> [Eggs]{
        return [
            Eggs(eggImage: "TELOR_1", unlocked: UserDefaultData.checkEggProgress(input: 0)),
            Eggs(eggImage: "TELOR_2", unlocked: UserDefaultData.checkEggProgress(input: 1)),
            Eggs(eggImage: "TELOR_3", unlocked: UserDefaultData.checkEggProgress(input: 2)),
            Eggs(eggImage: "TELOR_4", unlocked: UserDefaultData.checkEggProgress(input: 3)),
            Eggs(eggImage: "TELOR_5", unlocked: UserDefaultData.checkEggProgress(input: 4)),
            Eggs(eggImage: "TELOR_6", unlocked: UserDefaultData.checkEggProgress(input: 5)),
            Eggs(eggImage: "TELOR_7", unlocked: UserDefaultData.checkEggProgress(input: 6)),
            Eggs(eggImage: "TELOR_8", unlocked: UserDefaultData.checkEggProgress(input: 7)),
            Eggs(eggImage: "TELOR_9", unlocked: UserDefaultData.checkEggProgress(input: 8)),
            Eggs(eggImage: "TELOR_10", unlocked: UserDefaultData.checkEggProgress(input: 9)),
            Eggs(eggImage: "TELOR_11", unlocked: UserDefaultData.checkEggProgress(input: 10)),
            Eggs(eggImage: "TELOR_12", unlocked: UserDefaultData.checkEggProgress(input: 11))
        ]
    }
}


