//
//  Achievement_ViewController.swift
//  TIMLO_putu
//
//  Created by Putu Andhika on 31/05/20.
//  Copyright © 2020 Putu Andhika. All rights reserved.
//

import UIKit
import GameKit

class Achievement_ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var boardView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var array: [GKAchievement]!
    var arraydesc: [GKAchievementDescription]!
    var arrayss: [String] = [""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.clear.withAlphaComponent(0.25)
        boardView.backgroundColor = .init(red: 237, green: 235, blue: 232, alpha: 100)
        boardView.layer.cornerRadius = 30
        tableView.backgroundColor = .init(red: 237, green: 235, blue: 232, alpha: 100)
//        authenticateUser()
        checkingAll()
        
        GKAchievement.loadAchievements { (achievement, error) in
            self.array = achievement
            DispatchQueue.main.async {
                self.tableView.reloadData()
                // print("1. \(self.array.count)")
                self.filling()
            }
        }
        
        GKAchievementDescription.loadAchievementDescriptions { (achievement, error) in
            self.arraydesc = achievement
            DispatchQueue.main.async {
                self.tableView.reloadData()
//                // print("2. \(self.arraydesc.count)")
            }
        }
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func filling(){
        if array != nil {
            for item in array {
                arrayss.append(String(item.identifier))
            }
        }
        // print("this is array 1 \(arrayss)")
        arrayss.remove(at: 0)
    }
    
    func checkingAll(){
        let chargingTime: Int = UserDefaultData.checkChargingProgress()
        
        // print("CHARGINGGGGGGGGG\(chargingTime)")
        
        var achievement = GKAchievement(identifier: "charge3times")
        achievement.percentComplete = Double(chargingTime) * 34
        var value = Double(chargingTime) * 34
        if value >= 100{
            UserDefaultData.saveItemGet(Input: 9)
        }
        achievement.showsCompletionBanner = true
        GKAchievement.report([achievement]) { (error) in
            guard error == nil else {
                // print(error?.localizedDescription ?? "")
                return
            }
            // print("done1")
        }
        achievement = GKAchievement(identifier: "charge6times")
        achievement.percentComplete = Double(chargingTime) * 17
        value = Double(chargingTime) * 17
        if value >= 100{
            UserDefaultData.saveItemGet(Input: 6)
        }
        achievement.showsCompletionBanner = true
        GKAchievement.report([achievement]) { (error) in
            guard error == nil else {
                // print(error?.localizedDescription ?? "")
                return
            }
            // print("done2")
        }
        achievement = GKAchievement(identifier: "charge9times")
        achievement.percentComplete = Double(chargingTime) * 12
        value = Double(chargingTime) * 12
        if value >= 100{
            UserDefaultData.saveItemGet(Input: 1)
        }
        achievement.showsCompletionBanner = true
        GKAchievement.report([achievement]) { (error) in
            guard error == nil else {
                // print(error?.localizedDescription ?? "")
                return
            }
            // print("done3")
        }
        achievement = GKAchievement(identifier: "charge12times")
        achievement.percentComplete = Double(chargingTime) * 9
        value = Double(chargingTime) * 9
        if value >= 100{
            UserDefaultData.saveItemGet(Input: 7)
        }
        achievement.showsCompletionBanner = true
        GKAchievement.report([achievement]) { (error) in
            guard error == nil else {
                // print(error?.localizedDescription ?? "")
                return
            }
            // print("done4")
        }
        achievement = GKAchievement(identifier: "charge15times")
        achievement.percentComplete = Double(chargingTime) * 7
        value = Double(chargingTime) * 7
        if value >= 100{
            UserDefaultData.saveItemGet(Input: 4)
        }
        achievement.showsCompletionBanner = true
        GKAchievement.report([achievement]) { (error) in
            guard error == nil else {
                // print(error?.localizedDescription ?? "")
                return
            }
            // print("done5")
        }
    }
//    func authenticateUser() {
//        let player = GKLocalPlayer.local
//        player.authenticateHandler = { vc, error in
//            guard error == nil else {
//                // print(error?.localizedDescription ?? "")
//                return
//            }
//            if let vc = vc{
//                self.present(vc, animated: true, completion: nil)
//            }
//
//        }
//    }
    
    
    
    @IBAction func show_Achievement(_ sender: Any) {
        let vc = GKGameCenterViewController()
        vc.gameCenterDelegate = self
        vc.viewState = .achievements
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func unlock_Achievement(_ sender: Any) {
        let chargingTime: Int = UserDefaultData.checkChargingProgress()
        
        // print("CHARGINGGGGGGGGG\(chargingTime)")
        
        var achievement = GKAchievement(identifier: "charge3times")
        achievement.percentComplete = Double(chargingTime) * 34
        achievement.showsCompletionBanner = true
        GKAchievement.report([achievement]) { (error) in
            guard error == nil else {
                // print(error?.localizedDescription ?? "")
                return
            }
            // print("done1")
        }
        achievement = GKAchievement(identifier: "charge6times")
        achievement.percentComplete = Double(chargingTime) * 17
        achievement.showsCompletionBanner = true
        GKAchievement.report([achievement]) { (error) in
            guard error == nil else {
                // print(error?.localizedDescription ?? "")
                return
            }
            // print("done2")
        }
        achievement = GKAchievement(identifier: "charge9times")
        achievement.percentComplete = Double(chargingTime) * 12
        achievement.showsCompletionBanner = true
        GKAchievement.report([achievement]) { (error) in
            guard error == nil else {
                // print(error?.localizedDescription ?? "")
                return
            }
            // print("done3")
        }
        achievement = GKAchievement(identifier: "charge12times")
        achievement.percentComplete = Double(chargingTime) * 9
        achievement.showsCompletionBanner = true
        GKAchievement.report([achievement]) { (error) in
            guard error == nil else {
                // print(error?.localizedDescription ?? "")
                return
            }
            // print("done4")
        }
        achievement = GKAchievement(identifier: "charge15times")
        achievement.percentComplete = Double(chargingTime) * 7
        achievement.showsCompletionBanner = true
        GKAchievement.report([achievement]) { (error) in
            guard error == nil else {
                // print(error?.localizedDescription ?? "")
                return
            }
            // print("done5")
        }
        
    }
    
    @IBAction func reset_Achievement(_ sender: Any) {
        UserDefaultData.resetChargingProgress()
        _ = GKAchievement.resetAchievements(completionHandler: reset_Achievement(_:))
        return
    }
    
    
    // MARK: - Table view data source
    
     func numberOfSections(in tableView: UITableView) -> Int {
         // #warning Incomplete implementation, return the number of sections
         if arraydesc != nil{
             return 1
         }
         else{
             return 0
         }
         
     }
     
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         // #warning Incomplete implementation, return the number of rows
         if arraydesc != nil{
             return arraydesc.count
         }
         else{
             return 0
         }
     }
     
     
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Achievement_TableViewCell
         
         let achievement = arraydesc[indexPath.row]
         
         var percent: Int = 0
         
         var index = 0
         let searchToSearch = achievement.identifier
         
         //checking array[]
//         // print(arrayss)
         
         if arrayss != nil {
             let itemExists = arrayss.contains(where: {
                 $0.range(of: searchToSearch, options: .caseInsensitive) != nil
             })
//             // print(itemExists)
             
             if itemExists{
                 for item in arrayss {
                     // print("THIS IS INDEX\(index)")
                     // print(arrayss.count)
                     if item == searchToSearch {
//                         percent = 100
//                         // print(array[index])
//                         // print("PERCENTAGE\(array[index].percentComplete)")
                         percent = Int(array[index].percentComplete)
                        if percent >= 100 {
                            percent = 100
                        }
//                         // print("PERCENT\(percent)")
                         index += 1
                     }
                     else{
                        index += 1
                     }
                 }
             }
         }
         
         achievement.loadImage(completionHandler: { (imageachieved, error) in
             cell.image_Achievement.image = imageachieved
         })
         
         cell.title_Achievement.text = achievement.title
         cell.percent_Achievement.text = String(percent)+"%"
         cell.progressView.progress = Float(Double(percent)/100)
         
         return cell
     }

}
    extension Achievement_ViewController: GKGameCenterControllerDelegate {
        func gameCenterViewControllerDidFinish(_ gameCenterViewController:  GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
        }
    }
