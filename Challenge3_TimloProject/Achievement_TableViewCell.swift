//
//  Achievement_TableViewCell.swift
//  Challenge3_TimloProject
//
//  Created by Putu Andhika on 02/06/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class Achievement_TableViewCell: UITableViewCell {

    @IBOutlet weak var image_Achievement: UIImageView!
    @IBOutlet weak var title_Achievement: UILabel!
    @IBOutlet weak var percent_Achievement: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
