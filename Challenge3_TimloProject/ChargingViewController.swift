//
//  ChargingViewController.swift
//  Challenge3_TimloProject
//
//  Created by aldo on 29/05/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import BackgroundTasks

class ChargingViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource {
    var currentGame: GameScene?
    var cell : TipsCollectionViewCell?
    var battState : String = ""
    var stateCounter: Int = 0
    var eggStatus = true
    var countdown = 30
    var isLocked: Bool?
    var tipsCounter = 0
    var RNG = 0
    var timer:Timer?
    var timerNotif: TimeInterval!
    var alreadyPresent = false
    var eggs = Eggs.fetchEgg()
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    let userNotificationCenter = UNUserNotificationCenter.current()
    var alert: UIAlertController!
    var alertResult: UIAlertController!
    let dbCommon = ["GIRAFFE",  "CHICKEN", "GAJAH", "POLAR", "CRAB"]
    let dbRare = ["KOALA","OSTRICH","SLOTH","DODO"]
    let dbSuper = ["TREX","PEACOCK"]
    var random = ""
    var randomHolder = true
    var limiter = false
    
    
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var TipsCollectionView: UICollectionView!
    
    var tipsList = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tipsList = ["Enabling Airplane Mode can make your phone charge faster!",
                    "Reducing your brightness or Enabling Auto-brightness can make your battery last longer!",
                    "When your phone hits 20%, charge immediately! Don’t leave it below 20% or you will reduce your battery’s performance!",
                    "If you can’t charge your phone when it hits 20%, enable Low Power Mode!","Never ever drain out your phone’s battery to 0, ever! It will reduce your battery’s performance. We don’t want that, do we?","Careful! Your battery can’t handle extreme heat and cold! Always keep it between 0 Celcius - 35 Celcius to maintain its performance!","As often as you can, avoid direct sunlight, or leaving your phone in your car! If it often gets too hot, your phone’s performance can worsen!",
        "Have you use official Apple Charger? Using charger from somewhere else might transfer unstable electricity current to your phone!",
        "Using your MacBook Charger to charge your phone? Might sound like a quick shortcut! But to avoid overheating, change to your iPhone Charger now!","Disabling your background app refresh can make your phone work less harder, and it can save up more battery juice!","Don’t touch your phone when its charging! Playing with your phone when its charging can heat your battery up, and eventually reduce its performance.","Batteries have this memory of how many times your phone completed a 100% charge. If you charge your phone from 50%-100% twice, thats a whole cycle!","Your battery ‘age’ is not determined by how many days you own your device, but it is determined by Battery Life Cycle. The more often your charge your phone, the older your battery gets!","Apple has updated this cool feature that reads your sleeping habit, and to prevent your phone from overcharging when you charge it overnight! Its called Optimised Battery Charging!","Optimised Battery Charging read your regular sleeping schedule and determine when to continue charging so that when you wake up, it will be exactly 100%!"]
        TipsCollectionView.dataSource = self
        TipsCollectionView.delegate = self
        startTimer()
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
                   self.RNG = Int.random(in: 1 ..< 101)
               }
        if let view = self.view as! SKView? {
                                 // Load the SKScene from 'GameScene.sks'
                              let scene = GameScene(size: view.bounds.size)
                              scene.scaleMode = .resizeFill
                              view.ignoresSiblingOrder = true
                              view.presentScene(scene)
                                currentGame = scene as GameScene
                            currentGame?.chargingViewController = self
                             }
        progressBar.progress = UIDevice.current.batteryLevel
        progressBar.transform = progressBar.transform.scaledBy(x: 1, y: 20)
        let attrPercentageString = NSAttributedString(string: percentageLabel.text!, attributes: [
            NSAttributedString.Key.strokeColor: UIColor.orange,
         NSAttributedString.Key.strokeWidth: -2.0,
        ])
        let attrString = NSAttributedString(
         string: infoLabel.text!, attributes: [
             NSAttributedString.Key.strokeColor: UIColor.white,
             NSAttributedString.Key.strokeWidth: -2.0,
            ]
        )
         
        infoLabel.attributedText = attrString
        percentageLabel.attributedText = attrPercentageString
        let battery = CGFloat(UIDevice.current.batteryLevel*100)
               percentageLabel.text = String(Int((UIDevice.current.batteryLevel*100))) + "%"
               
               // print(battery)
               // print(CGFloat.pi * 2)
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
         Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
                   self.percentageLabel.text = String(Int((UIDevice.current.batteryLevel*100))) + "%"
                    let batteryNow = CGFloat(UIDevice.current.batteryLevel*100)
                   switch UIDevice.current.batteryState.rawValue {
                   case 0:
                       self.battState = "Unknown"
                   case 1:
                       self.battState = "UnPlugged"
                   case 2:
                       self.battState = "Charging"
                   case 3:
                       self.battState = "Full"
                   default:
                       break
                   }
                if(batteryNow >= 100){
                    if self.limiter == false {
                        self.limiter = true
                        var sumRec = defaults.integer(forKey: "SuccessRecord") + 1
                        var sumRecF = defaults.integer(forKey: "FailRecord") + 0
                        UserDefaultData.saveRecordSucc(Succ: sumRec)
                        UserDefaultData.saveRecordFail(Fail: sumRecF)
                        UserDefaultData.DataRecord(Srec: defaults.integer(forKey: "SuccessRecord") , Frec: defaults.integer(forKey: "FailRecord"), switcher: false)
                    }
                    if self.alreadyPresent == false {
                    self.alert = UIAlertController(title: "Your Egg is Hatching !", message: "Find out what animal you will get.", preferredStyle: .alert)
                    self.alert.addAction(UIAlertAction(title: "Hatch !", style: .default, handler: { (action: UIAlertAction!) in
                        self.alertResult = UIAlertController(title: "", message: "Congratulations", preferredStyle: .alert)
                        let imageView = UIImageView(frame: CGRect(x: 65, y: 65, width: 130, height: 180))
                        if 1 ... 10 ~= self.RNG {
                            if self.randomHolder == true {
                                self.random = self.dbSuper.randomElement()!
                                // print(self.random)
                                
                                imageView.image = UIImage(named: self.random )
                                self.alertResult.title = "Super Rare"
                                UserDefaultData.saveAnimalGet(animalInput: self.random)
                                self.randomHolder = false
                            }
                            
                        }
                        else if 11 ... 40 ~= self.RNG {
                            if self.randomHolder == true {
                                self.random = self.dbRare.randomElement()!
                                imageView.image = UIImage(named: self.random )
                                self.alertResult.title = "Rare"
                                // print(self.random)
                                
                                UserDefaultData.saveAnimalGet(animalInput: self.random)
                                self.randomHolder = false
                            }
                            
                        }
                        else if 41 ... 100 ~= self.RNG {
                            if self.randomHolder == true {
                                self.random = self.dbCommon.randomElement()!
                                imageView.image = UIImage(named: self.random)
                                self.alertResult.title = "Common"
                                // print(self.random)
                                
                                UserDefaultData.saveAnimalGet(animalInput: self.random)
                                self.randomHolder = false
                            }
                            
                        }
                        self.alertResult.view.addSubview(imageView)
                        self.alertResult.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                            self.performSegue(withIdentifier: "unwindToHome", sender: nil)}))
                        UserDefaultData.saveChargingProgress()
                        let height = NSLayoutConstraint(item: self.alertResult.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 320)
                        let width = NSLayoutConstraint(item: self.alertResult.view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
                        self.alertResult.view.addConstraint(height)
                        self.alertResult.view.addConstraint(width)
                        self.present(self.alertResult, animated: true)
                    }))
                    let imageView = UIImageView(frame: CGRect(x: 10, y: 50, width: 250, height: 250))
                    imageView.image = UIImage(named: self.eggs[UserDefaultData.checkSelectedEgg()].eggImage)
                    self.alert.view.addSubview(imageView)
                    let height = NSLayoutConstraint(item: self.alert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 320)
                    let width = NSLayoutConstraint(item: self.alert.view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
                    self.alert.view.addConstraint(height)
                    self.alert.view.addConstraint(width)
                    self.present(self.alert, animated: true)
                    }
                    self.alreadyPresent = true
                }
                if(self.battState == "Charging"){
                         switch UIApplication.shared.applicationState {
                           case .active:
                            
                            self.cd = 11
                            self.temp = true
                             //// print("App currently active")
                           case .background:
                            self.timerNotif = TimeInterval(UIApplication.shared.backgroundTimeRemaining)
                             //// print("App is backgrounded.")
                             // print("Background time remaining = " +
//                             "\(UIApplication.shared.backgroundTimeRemaining) seconds")
                             if self.tempVar == true {
                                 self.sendNotification()
                                 self.stateCounter = 1
                                 break
                             }
                             break
                           case .inactive:
                             
                             //// print("App is Inactive")
                             if self.tempVar == true {
                                 self.sendNotification()
                                 self.stateCounter = 1
                                 break
                             }
                    }
                         self.infoLabel.text = self.battState
                         self.currentGame?.animateChar()
                    if(self.timerNotif == 0){
                        
                        if self.limiter == false {
                            self.limiter = true
                            var sumRecF = defaults.integer(forKey: "FailRecord") + 1
                            UserDefaultData.saveRecordFail(Fail: sumRecF)
                            UserDefaultData.DataRecord(Srec: defaults.integer(forKey: "SuccessRecord") , Frec: defaults.integer(forKey: "FailRecord"), switcher: false)
                        }
                        self.backToHome(self)
                    }
                     }
                if(self.battState == "UnPlugged"){
                                     if (self.eggStatus == true) {
                                         self.countdown = 30
                                         self.showAlert()
                                     }
                                     self.infoLabel.text = self.battState
                                 self.currentGame?.charMoveEnded()
                                 
                                }
                                self.progressBar.setProgress(Float(UIDevice.current.batteryLevel), animated: true)
                    self.currentGame?.animateChar()
                           }
                    
    }
    func stopTimer() {
        timer?.invalidate()
        
    }
    
    func backToHome(_ sender: Any) {
        let sharedDefaults = UserDefaults(suiteName: "group.com.chargeit.widget")
        sharedDefaults?.setValue("NotCharging", forKey: "playKey")
        self.performSegue(withIdentifier: "unwindToHome", sender: self)
    }
    var cd = 11
    var temp = true
    var tempVar: Bool!
    @objc func appMovedToBackground() {
       registerBackgroundTask()
        self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            if self.cd > 0 && self.temp == true {
                self.cd = self.cd - 1
                // print(self.cd)
            }
            if self.cd == 0 {
                if self.temp == true {
                    self.temp = false
                    self.backgroundProc()
                    self.stopTimer()
                }
            }
        }
    }
    func backgroundProc() {
        if (isLocked == false) {
            // print("Locked Screen !")
            tempVar = false
            //code yang locked screen
        }
        else {
            // print("Not Locked !")
            stateCounter = 0
            tempVar = true
            //registerBackgroundTask()
            //// print("Welcome to the background world. . .")
            //sendNotification()
        }
    }
    
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != .invalid)
        
    }
    
    func endBackgroundTask() {
        // print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = .invalid

    }
    
    func showAlert() {
        alert = UIAlertController(title: "HEY! Are you sure you want to stop charging?", message: "You will crack your Egg prematurely! Quick - plug it back in to save your battery and Egg! \n Time Remaining \(countdown) second(s)", preferredStyle: .alert)
        //alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true){
            Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
                if self.countdown > 0 {
                    self.countdown = self.countdown - 1
                    // print(self.countdown)
                    if UIDevice.current.batteryState.rawValue == 2{
                        self.alert.dismiss(animated: true, completion: nil)
                        self.eggStatus = true
                    }
                   
                }
             self.alert.message = "You will crack your Egg prematurely! Quick - plug it back in to save your battery and Egg! \n Time Remaining \(self.countdown) second(s)"
                if self.countdown == 0 && self.eggStatus == false{
                    self.alert.dismiss(animated: true, completion: nil)
                    if self.limiter == false {
                        self.limiter = true
                        var sumRecF = defaults.integer(forKey: "FailRecord") + 1
                        UserDefaultData.saveRecordFail(Fail: sumRecF)
                        UserDefaultData.DataRecord(Srec: defaults.integer(forKey: "SuccessRecord") , Frec: defaults.integer(forKey: "FailRecord"), switcher: false)
                    }
                    self.backToHome(self)
                }
            }
        }
        
        
        //let when = DispatchTime.now() + 30
        //DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            self.eggStatus = false
           // self.alert.dismiss(animated: true, completion: nil)
        //}
    }
    func sendNotification() {
        if stateCounter == 0 {
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "Whoa-whoa! Using your phone while charging?"
        notificationContent.body = "Remember, it will surely heat up your battery! You will crack your Egg prematurely! Hands-off!"
        notificationContent.badge = NSNumber(value: 1)
        
        if let url = Bundle.main.url(forResource: "dune",
                                    withExtension: "png") {
            if let attachment = try? UNNotificationAttachment(identifier: "dune",
                                                            url: url,
                                                            options: nil) {
                notificationContent.attachments = [attachment]
            }
        }
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1,
                                                        repeats: false)
        let request = UNNotificationRequest(identifier: "testNotification",
                                            content: notificationContent,
                                            trigger: trigger)
        
        userNotificationCenter.add(request) { (error) in
            if let error = error {
                // print("Notification Error: ", error)
            }
            }
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        return tipsList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "viewCellChallenge", for: indexPath) as! TipsCollectionViewCell

        // set nilai ke view dalam cell
        // print("asek")
        let image = tipsList[indexPath.row]
        cell.tipsLabel.text = image

        return cell
    }

    /**
       Scroll to Next Cell
       */
      func startTimer() {

        Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
      }


      @objc func scrollAutomatically(_ timer1: Timer) {

        if tipsCounter < tipsList.count {
            let index = IndexPath.init(item: tipsCounter, section: 0)
            self.TipsCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            tipsCounter += 1
        }
        else{
            tipsCounter = 0
            let index = IndexPath.init(item: tipsCounter, section: 0)
            self.TipsCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
        }
      }
    
    func background() {
        BGTaskScheduler.shared.register(forTaskWithIdentifier: "com.apple.chargeitReceive", using: nil){task in
           
        }
    }
    func scheduleAppRefresh() {
       let request = BGAppRefreshTaskRequest(identifier: "com.apple.chargeitSend")
       // Fetch no earlier than 15 minutes from now
       request.earliestBeginDate = Date(timeIntervalSinceNow: 15 * 60)
            
       do {
          try BGTaskScheduler.shared.submit(request)
       } catch {
          print("Could not schedule app refresh: \(error)")
       }
    }


}
