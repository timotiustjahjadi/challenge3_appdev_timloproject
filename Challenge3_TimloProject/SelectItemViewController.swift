//
//  SelectItemViewController.swift
//  Challenge3_TimloProject
//
//  Created by Timotius Tjahjadi  on 29/05/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class SelectItemViewController: UIViewController {

    @IBOutlet weak var itemCollectionView: UICollectionView!
    @IBOutlet weak var EggDisplayImageView: UIImageView!
    @IBOutlet weak var ItemOnEggImageView: UIImageView!
    @IBOutlet weak var ItemsLabel: UILabel!
    @IBOutlet weak var ItemsContainerView: UIView!
    @IBOutlet weak var PreviousButton: UIButton!
    @IBOutlet weak var NextButton: UIButton!
    
    @IBAction func NextButtonAction(_ sender: Any) {
        itemCollectionView.reloadData()
        if ItemsLabel.text == "Items"{
            ItemsLabel.text = "Eggs"
        }
        else{
            ItemsLabel.text = "Items"
        }
    }
    
    @IBAction func PreviousButtonAction(_ sender: Any) {
        itemCollectionView.reloadData()
        if ItemsLabel.text == "Items"{
            ItemsLabel.text = "Eggs"
        }
        else{
            ItemsLabel.text = "Items"
        }
    }
    
//    @IBAction func ExitButtonAction(_ sender: Any) {
//        self.navigationController?.popViewController(animated: false)
//    }
    
    var selectedItems: IndexPath!
    var selectedItemsEgg: IndexPath!
    
    var selectedItem: Int = 0
    var selectedEgg: Int = 0
    
    var items = Items.fetchItem()
    var eggs = Eggs.fetchEgg()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Initial Item and Egg on Display
        ItemOnEggImageView.image = UIImage(named: items[UserDefaultData.checkSelectedItem()].itemOnEggImage)
        EggDisplayImageView.image = UIImage(named: eggs[UserDefaultData.checkSelectedEgg()].eggImage)
        //
        
        let layout = itemCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        layout.itemSize = CGSize(width: 75, height: 75)
        
        //MARK: Change the BG into Gradient Color
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor.init(red: 0.714, green: 0.82, blue: 0.82, alpha: 1).cgColor, UIColor.init(red: 0.74, green: 0.83, blue: 0.52, alpha: 1).cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        //
        
        ItemsContainerView.backgroundColor = UIColor(red: 0.9, green: 0.89, blue: 0.87, alpha: 1)
        
        //MARK: Put outline to the Animal Label
        let strokeTextAttributes = [
          NSAttributedString.Key.strokeColor : UIColor.white,
          NSAttributedString.Key.foregroundColor : UIColor.systemOrange,
          NSAttributedString.Key.strokeWidth : -4.0,
          NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 40)]
          as [NSAttributedString.Key : Any]
        
        ItemsLabel.attributedText = NSMutableAttributedString(string: "Animals Name", attributes: strokeTextAttributes)
        //
        
        ItemsLabel.text = "Items"
        ItemsLabel.font = UIFont(name: "BowlbyOneSC-Regular",size:30)
        ItemsLabel.textColor = UIColor(red: 0.9, green: 0.54, blue: 0.34, alpha: 1)
        ItemsLabel.adjustsFontForContentSizeCategory = true

        
        itemCollectionView.dataSource = self
        itemCollectionView.delegate = self
    }

}

extension SelectItemViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if ItemsLabel.text == "Items"{
            return items.count
        }
        else{
            return eggs.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemEgg", for: indexPath) as! ItemCollectionViewCell

        if ItemsLabel.text == "Items"{
            let item = items[indexPath.item]
            cell.item = item
            cell.activated = true
            
            let getData = UserDefaultData.checkSelectedItem()
            if indexPath.item == getData{
                cell.BoxItemImageView.image = UIImage(named: "Item Square Selected")
            }
            else{
                cell.BoxItemImageView.image = UIImage(named: "Item Square")
            }
            
            //MARK: Checking unlocked or not
            if items[indexPath.item].unlocked == false {
                cell.isUserInteractionEnabled = false
                cell.BoxItemImageView.image = UIImage(named: "Item Square")
            }
            else{
                cell.isUserInteractionEnabled = true
            }
            //
            
        }
        else{
            let egg = eggs[indexPath.item]
            cell.egg = egg
            cell.activated = false
            
            let getData = UserDefaultData.checkSelectedEgg()
            if indexPath.item == getData{
                cell.BoxItemImageView.image = UIImage(named: "Item Square Selected")
            }
            else{
                cell.BoxItemImageView.image = UIImage(named: "Item Square")
            }
            
            //MARK: Checking unlocked or not
            if eggs[indexPath.item].unlocked == false {
                cell.isUserInteractionEnabled = false
                cell.BoxItemImageView.image = UIImage(named: "Item Square")

            }
            else{
                cell.isUserInteractionEnabled = true
            }
            //
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if ItemsLabel.text == "Items"{
            self.ItemOnEggImageView.image = UIImage(named: items[indexPath.item].itemOnEggImage)

            selectedItem = indexPath.item
            UserDefaultData.saveSelectedAItem(selectedItem: selectedItem)
            collectionView.reloadData()
        }
        else{
            self.EggDisplayImageView.image = UIImage(named: eggs[indexPath.item].eggImage)

            selectedEgg = indexPath.item
            UserDefaultData.saveSelectedEgg(selectedEgg: selectedEgg)
            collectionView.reloadData()
        }
        
    }
}
