//
//  Animal.swift
//  BatteryApp
//
//  Created by Timotius Tjahjadi  on 27/05/20.
//  Copyright © 2020 Learn. All rights reserved.
//

import UIKit

class Items{
    var name = ""
    var itemImage = ""
    var itemOnEggImage = ""
    var unlocked: Bool
    
    init(name: String, itemImage: String, itemOnEggImage: String, unlocked: Bool){
        self.name = name
        self.itemImage = itemImage
        self.itemOnEggImage = itemOnEggImage
        self.unlocked = unlocked
    }
    
    static func fetchItem() -> [Items]{
        return [
            Items(name: "None", itemImage: "ITEMS_NONE_DISPLAY", itemOnEggImage: "none", unlocked: true),
            Items(name: "Bag", itemImage: "ITEMS_BAG_DISPLAY", itemOnEggImage: "ITEMS_BAG_ON EGG", unlocked: UserDefaultData.checkItemProgress(input: 0)),
            Items(name: "Bowtie", itemImage: "ITEMS_BOWTIE_DISPLAY", itemOnEggImage: "ITEMS_BOWTIE_ON EGG", unlocked: UserDefaultData.checkItemProgress(input: 1)),
            Items(name: "Camera", itemImage: "ITEMS_CAMERA_DISPLAY", itemOnEggImage: "ITEMS_CAMERA_ON EGG", unlocked: UserDefaultData.checkItemProgress(input: 2)),
            Items(name: "Chains", itemImage: "ITEMS_CHAINS_DISPLAY", itemOnEggImage: "ITEMS_CHAINS_ON EGG", unlocked: UserDefaultData.checkItemProgress(input: 3)),
            Items(name: "Crown", itemImage: "ITEMS_CROWN_DISPLAY", itemOnEggImage: "ITEMS_CROWN_ON EGG", unlocked: UserDefaultData.checkItemProgress(input: 4)),
            Items(name: "Glasses", itemImage: "ITEMS_GLASSES_DISPLAY", itemOnEggImage: "ITEMS_GLASSES_ON EGG", unlocked: UserDefaultData.checkItemProgress(input: 5)),
            Items(name: "Hat", itemImage: "ITEMS_HAT_DISPLAY", itemOnEggImage: "ITEMS_HAT_ON EGG", unlocked: UserDefaultData.checkItemProgress(input: 6)),
            Items(name: "Mustache", itemImage: "ITEMS_MUSTACHE_DISPLAY", itemOnEggImage: "ITEMS_MUSTACHE_ON EGG", unlocked: UserDefaultData.checkItemProgress(input: 7)),
            Items(name: "Pipe", itemImage: "ITEMS_PIPE_DISPLAY", itemOnEggImage: "ITEMS_PIPE_ON EGG", unlocked: UserDefaultData.checkItemProgress(input: 8)),
            Items(name: "Sunray", itemImage: "ITEMS_SUNRAY_DISPLAY", itemOnEggImage: "ITEMS_SUNRAY_ON EGG", unlocked: UserDefaultData.checkItemProgress(input: 9))
        ]
    }
}


