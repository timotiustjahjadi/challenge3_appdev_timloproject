//
//  AnalyticViewController.swift
//  Challenge3_TimloProject
//
//  Created by Timotius Tjahjadi  on 17/06/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class AnalyticViewController: UIViewController {
    @IBOutlet weak var BarGraphCollectionView: UICollectionView!
    @IBOutlet weak var ClassificationCollectionView: UICollectionView!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var WeekXReportLabel: UILabel!
    @IBOutlet weak var FromXXLabel: UILabel!
    @IBOutlet weak var SuccessNumLabel: UILabel!
    @IBOutlet weak var InterruptedNumLabel: UILabel!
    @IBOutlet weak var SuccessLabel: UILabel!
    @IBOutlet weak var InterruptedLabel: UILabel!
    @IBOutlet weak var ClassLabel: UILabel!
    @IBOutlet weak var LightLabel: UILabel!
    @IBOutlet weak var MediumLabel: UILabel!
    @IBOutlet weak var HeavyLabel: UILabel!
    @IBOutlet weak var BulletinCloseBtn: UIButton!
    @IBOutlet weak var ShadowView: UIView!
    @IBOutlet weak var BulletinBoardView: UIView!
    @IBOutlet weak var InfoBtn: UIButton!
    
    let defaults = UserDefaults.standard
    var scrollToFlag: Bool = false
    var selectedItems: Int!
    var WeekScoreValues: [Int] = []
    var SuccValues: [Int] = []
    var FailValues: [Int] = []
    let classValue: [Int] = [5, 6, 4, 7, 6, 8, 6, 10, 4, 4, 5, 7, 6, 5, 7]
    
    @IBAction func ExitButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func InfoBtns(_ sender: Any) {
        BulletinBoardView.isHidden = false
        ShadowView.isHidden = false
    }
    
    @IBAction func BulletinCloseBtns(_ sender: Any) {
        BulletinBoardView.isHidden = true
        ShadowView.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SuccValues = UserDefaultData.getDataSuccess()
        FailValues = UserDefaultData.getDataFail()
        // print("LAST ELEMENT TESTING(S) : ",SuccValues.last)
        // print("LAST ELEMENT TESTING(F) : ",FailValues.last)
        
        BulletinCloseBtn.layer.cornerRadius = BulletinCloseBtn.layer.frame.width / 2
        
        var index = 0
        for data in SuccValues{
            WeekScoreValues.append(ScoreCounting(succ: data, fail: FailValues[index]))
            index += 1
        }
        
        //MARK: Change the BG into Gradient Color
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor.init(red: 0.714, green: 0.82, blue: 0.82, alpha: 1).cgColor, UIColor.init(red: 0.74, green: 0.83, blue: 0.52, alpha: 1).cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        //
        
//        WeekScoreValues = defaults.array(forKey: "arrSuccess") as! [Int]
//        values2 = defaults.array(forKey: "arrFail") as! [Int]
        let getWeek = UserDefaults.standard.integer(forKey: "selectedBar")

        TitleLabel.font = UIFont(name: "BowlbyOneSC-Regular", size: 22)
        WeekXReportLabel.font = UIFont(name: "BowlbyOneSC-Regular", size: 17)
        WeekXReportLabel.text = String("Week \(getWeek + 1) report")
        WeekXReportLabel.textColor = UIColor(red: 215/255, green: 99/255, blue: 90/255, alpha: 1)
        FromXXLabel.font = UIFont(name: "BowlbyOneSC-Regular", size: 15)
        FromXXLabel.text = String("From \(SuccValues[getWeek] + FailValues[getWeek]) charges activity")
        SuccessNumLabel.font = UIFont(name: "BowlbyOneSC-Regular", size: 34)
        SuccessNumLabel.textColor = UIColor(red: 0.9, green: 0.54, blue: 0.34, alpha: 1)
        SuccessNumLabel.text = String(SuccValues[getWeek])
        InterruptedNumLabel.font = UIFont(name: "BowlbyOneSC-Regular", size: 34)
        InterruptedNumLabel.textColor = UIColor(red: 0.9, green: 0.54, blue: 0.34, alpha: 1)
        InterruptedNumLabel.text = String(FailValues[getWeek])
        SuccessLabel.font = UIFont(name: "BowlbyOneSC-Regular", size: 13)
        InterruptedLabel.font = UIFont(name: "BowlbyOneSC-Regular", size: 13)
        ClassLabel.font = UIFont(name: "BowlbyOneSC-Regular", size: 15)
        LightLabel.font = UIFont(name: "BowlbyOneSC-Regular", size: 10)
        MediumLabel.font = UIFont(name: "BowlbyOneSC-Regular", size: 10)
        HeavyLabel.font = UIFont(name: "BowlbyOneSC-Regular", size: 10)
        view.backgroundColor = UIColor.tertiarySystemGroupedBackground
        BarGraphCollectionView.dataSource = self
        BarGraphCollectionView.delegate = self
        ClassificationCollectionView.dataSource = self
        ClassificationCollectionView.delegate = self
    }
    
}
//var saveRat: CGFloat!
//var get: Int!

func ScoreCounting(succ: Int, fail: Int) -> Int {
    let totalScore = (succ * 500) - (fail * 300)
    
    if totalScore>0 {
        return totalScore
    }
    else{
        return 0
    }
}

extension AnalyticViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == ClassificationCollectionView {
            return 15
        }
        else{
            return WeekScoreValues.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == ClassificationCollectionView {
            return 3
        }
        else{
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let get = UserDefaults.standard.integer(forKey: "selectedBar")
        
        if collectionView == ClassificationCollectionView{
            
            let border = collectionView.frame.height
            let max = classValue.max()!
            
            let ratio = CGFloat(max) / border
            
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "classItem", for: indexPath) as! ClassificationCollectionViewCell
            cell2.bars.widthAnchor.constraint(equalToConstant: ClassificationCollectionView.frame.width/18).isActive = true
//            cell2.bars.heightAnchor.constraint(equalToConstant: 23.0).isActive = true
            cell2.bars.alpha = 0.2
            cell2.bars.backgroundColor = UIColor(red: 0.9, green: 0.54, blue: 0.34, alpha: 1)
            
            cell2.bar2Height.constant = CGFloat(classValue[indexPath.item]) / ratio
            
            let total = SuccValues[get] + FailValues[get]
            
            for _ in 0...14 {
                if total < 3 {
                    if indexPath.item == 0 {
                        cell2.bars.alpha = 1
                    }
                }
                else if total >= 3 && total <= 16{
                    if indexPath.item == total - 2 {
                        cell2.bars.alpha = 1
                    }
                }
                else{
                    cell2.bars.alpha = 1
                }
            }
            
            return cell2
        }
        
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "barChartItem", for: indexPath) as! AnalyticCollectionViewCell
            let border = collectionView.frame.height - 120 - 100
            let max = WeekScoreValues.max()!
            
            let ratio = max / Int(border)
            
            if indexPath.row == get {
                cell.backgroundColor = UIColor.white
                cell.layer.shadowColor = UIColor.black.cgColor
                cell.layer.shadowOffset = CGSize(width: 0, height: 0)
                cell.layer.shadowRadius = 8.0
                cell.layer.shadowOpacity = 0.1
                cell.layer.masksToBounds = false
                cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
//                cell.BarScoreLabel.text = String(Int(WeekScoreValues[indexPath.item]))
                let totalScore = ScoreCounting(succ: SuccValues[indexPath.item], fail: FailValues[indexPath.item])
                cell.BarScoreLabel.text = String(totalScore)
//                cell.BarWeekLabel.textColor = UIColor.black
            }
            else{
                cell.backgroundColor = UIColor.clear
                cell.layer.shadowColor = UIColor.clear.cgColor
                cell.BarScoreLabel.text = ""
            }
            cell.BarScoreLabel.font = UIFont(name: "BowlbyOneSC-Regular",size:17)
            cell.BarScoreLabel.textColor = UIColor(red: 0.9, green: 0.54, blue: 0.34, alpha: 1)
            
            cell.BarWeekLabel.font = UIFont(name: "BowlbyOneSC-Regular",size:17)
            
            cell.BarWeekLabel.text = String("Week \(indexPath.item + 1)")
            var height: CGFloat
            
            if ratio <= 0 {
                height = 0
            }
            else{
                height = CGFloat(WeekScoreValues[indexPath.item] / ratio)
            }
            if height <= 0 {
                cell.BarHeight.constant = 0
            }
            else{
                cell.BarHeight.constant = height
            }
            
            cell.BarView.backgroundColor = UIColor(red: 143/255, green: 189/255, blue: 194/255, alpha: 1)
            if scrollToFlag == false {
                if get == WeekScoreValues.count-1 {
                    BarGraphCollectionView.scrollToItem(at: IndexPath(item: get, section: 0), at: .right, animated: true)
                }
                else{
                    BarGraphCollectionView.scrollToItem(at: IndexPath(item: get+1, section: 0), at: .right, animated: true)
                }
                scrollToFlag = true
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == ClassificationCollectionView {
            return CGSize(width: ClassificationCollectionView.frame.width/18, height: ClassificationCollectionView.frame.height)

        }
        else{
            return CGSize(width: BarGraphCollectionView.frame.width/5, height: BarGraphCollectionView.frame.height-80)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == BarGraphCollectionView {
            selectedItems = indexPath.item
            UserDefaults.standard.set(selectedItems, forKey: "selectedBar")
            WeekXReportLabel.text = String("Week \(indexPath.item + 1) report")
            SuccessNumLabel.text = String(SuccValues[indexPath.item])
            InterruptedNumLabel.text = String(FailValues[indexPath.item])
            FromXXLabel.text = String("From \(SuccValues[indexPath.item] + FailValues[indexPath.item]) charges activity")
            BarGraphCollectionView.reloadData()
            ClassificationCollectionView.reloadData()
        }
    }
}

