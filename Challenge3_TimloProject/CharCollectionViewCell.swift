//
//  CharCollectionViewCell.swift
//  BatteryApp
//
//  Created by Timotius Tjahjadi  on 27/05/20.
//  Copyright © 2020 Learn. All rights reserved.
//

import UIKit

class CharCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var AnimalImageView: UIImageView!
    
    var animal: Animal! {
        didSet{
            self.updateUI()
        }
    }
    
    func updateUI(){
        if let animal = animal {
            AnimalImageView.image = UIImage(named: animal.animalImage)
            
            //MARK: Change the animal Color
            if animal.unlocked == false {
                let templateImage =  AnimalImageView.image?.withRenderingMode(.alwaysTemplate)
                AnimalImageView.image = templateImage
                AnimalImageView.tintColor = UIColor.black
            }
            //
        }
        else{
            AnimalImageView.image = nil
        }
        
    }
}
