//
//  ClassificationCollectionViewCell.swift
//  Challenge3_TimloProject
//
//  Created by Timotius Tjahjadi  on 19/06/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class ClassificationCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bars: UIButton!{
        didSet{
            bars.layer.cornerRadius = 8
        }
    }
    @IBOutlet weak var bar2Height: NSLayoutConstraint!
}
