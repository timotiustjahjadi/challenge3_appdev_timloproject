//
//  ViewController.swift
//  Challenge3_TimloProject
//
//  Created by Timotius Tjahjadi  on 22/05/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import GameKit

class ViewController: UIViewController {
    var currentGame: GameScene?
    let userNotificationCenter = UNUserNotificationCenter.current()
    var battState : String = ""
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    var stateCounter: Int = 0
    var eggStatus = true
    var countdown = 30
    var gameTimer: Timer?
    
    var canClicked: Bool!
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
   
    @IBOutlet weak var characterButton: UIButton!
    @IBOutlet weak var achievementButton: UIButton!
    @IBAction func DummyBtn(_ sender: Any) {
        defaults.set([7,5,8,6,8,8], forKey: "arrSuccess")
        defaults.set([1,0,2,1,6,1], forKey: "arrFail")
        
        let alert = UIAlertController(title: "Dummy Data Added", message: "Check Your Analytic", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Great!", style: .default, handler: nil))

        self.present(alert, animated: true)
        // print("TEST : ", defaults.array(forKey: "arrSuccess"))
        // print("TEST : ", defaults.array(forKey: "arrFail"))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        authenticateUser()
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let sharedDefaults = UserDefaults(suiteName: "group.com.chargeit.widget")
                   sharedDefaults?.setValue("NotCharging", forKey: "playKey")
        //MARK: Set initial Default Animal
        if UserDefaultData.isAppAlreadyLaunchedOnce() == false{
            UserDefaultData.saveAnimalProgress(animalUnlockedList: ["T", "F", "F", "F", "F", "F", "F", "F", "F","F","F"])
            UserDefaultData.saveEggProgress(eggUnlockedList: ["T", "T", "T", "T", "F", "T", "F", "T", "F", "T", "T", "F"])
            UserDefaultData.saveItemProgress(itemUnlockedList: ["T", "F", "T", "T", "F", "T", "F", "F", "T", "F"])
            //First time
            UserDefaultData.dailyDate(date: dateFormatter.string(from: date))
        }
        //Compare Date
        if  defaults.string(forKey: "LastDate") == dateFormatter.string(from: date){
            //if LastDate = date now
            // print("Do nothing")
        }
        else {
            // print("Do something here")
            //Maybe Bikin variable total days of using this app.
            //var Day+1
            if defaults.integer(forKey: "DayPassed") == 7 {
                var day = defaults.integer(forKey: "DayPassed") + 1
                UserDefaultData.DayCount(Day: day, Reset: true)
                UserDefaultData.saveWeeklyRecord(WSucc: defaults.integer(forKey: "SuccessRecord"), WFail: defaults.integer(forKey: "FailRecord"))
                UserDefaultData.DataRecord(Srec: defaults.integer(forKey: "SuccessRecord") , Frec: defaults.integer(forKey: "FailRecord"), switcher: true)
                //Reset Record
                UserDefaultData.saveRecordSucc(Succ: 0)
                UserDefaultData.saveRecordFail(Fail: 0)
            }
            else {
                var day = defaults.integer(forKey: "DayPassed") + 1
                UserDefaultData.DayCount(Day: day, Reset: false)
                UserDefaultData.DataRecord(Srec: defaults.integer(forKey: "SuccessRecord") , Frec: defaults.integer(forKey: "FailRecord"), switcher: false)
            }
            //if day = 7 save fail & success ke UserDefaultData.SaveWeekly()
            //Day reset -> 0
            //Reset Record -> 0
            //Call DataProcess()
        }
        UserDefaultData.dailyDate(date: dateFormatter.string(from: date))
        
        // print(UserDefaultData.checkChargingProgress())
               if let view = self.view as! SKView? {
                   // Load the SKScene from 'GameScene.sks'
                let scene = GameScene(size: view.bounds.size)
                scene.scaleMode = .resizeFill
                view.ignoresSiblingOrder = true
                view.presentScene(scene)
                
                   currentGame = scene as GameScene
                currentGame?.viewController = self
                
                let notificationCenter = NotificationCenter.default
                notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
                
               }
        
        
        
                let attrString = NSAttributedString(
         string: infoLabel.text!, attributes: [
             NSAttributedString.Key.strokeColor: UIColor.white,
             NSAttributedString.Key.strokeWidth: -2.0,
            ]
        )
         
        infoLabel.attributedText = attrString
         
               switch UIDevice.current.batteryState.rawValue {
               case 0:
                   battState = "Unknown"
               case 1:
                   battState = "UnPlugged"
               case 2:
                   battState = "Charging"
               case 3:
                   battState = "Full"
               default:
                   break
               }
               let battery = CGFloat(UIDevice.current.batteryLevel*100)
//               percentageLabel.text = String(Int((UIDevice.current.batteryLevel*100))) + "%"
               
               // print(battery)
               // print(CGFloat.pi * 2)
        
               Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
//                   self.percentageLabel.text = String(Int((UIDevice.current.batteryLevel*100))) + "%"
                 let batteryNow = CGFloat(UIDevice.current.batteryLevel*100)
                   switch UIDevice.current.batteryState.rawValue {
                   case 0:
                       self.battState = "Unknown"
                   case 1:
                       self.battState = "UnPlugged"
                   case 2:
                       self.battState = "Charging"
                   case 3:
                       self.battState = "Full"
                   default:
                       break
                   }
                   if(self.battState == "Charging" && batteryNow <= 50){

                    self.canClicked = true
                    
                       self.startButton.isEnabled = true
//
                          }
                   else if(self.battState == "UnPlugged" || batteryNow > 50){
                       
                    self.canClicked = false
                    
                        self.startButton.isEnabled = true
                    self.currentGame?.charMoveEnded()
                    
                   }
                  
               }
        // print("Success : \(defaults.integer(forKey: "SuccessRecord"))")
        // print("Failed : \(defaults.integer(forKey: "FailRecord"))")
           }

           override var shouldAutorotate: Bool {
               return true
           }
    func authenticateUser() {
        let player = GKLocalPlayer.local
        player.authenticateHandler = { vc, error in
            guard error == nil else {
                // print(error?.localizedDescription ?? "")
                return
            }
            if let vc = vc{
                self.present(vc, animated: true, completion: nil)
            }
            
        }
    }
    @IBAction func btnStartClicked(_ sender: Any) {
        if canClicked == true {
            let sharedDefaults = UserDefaults(suiteName: "group.com.chargeit.widget")
            sharedDefaults?.setValue("Charging", forKey: "playKey")
            if let vc = storyboard?.instantiateViewController(identifier: "ChargingID") as? ChargingViewController{
                self.show(vc, sender: self)
            }
        }
        else{
            let alert = UIAlertController(title: "Check Your Charger or Battery", message: "You can only start hatching the egg if you plugged your charger or your battery is below 50%", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Sure!", style: .default, handler: nil))

            self.present(alert, animated: true)

        }
        
    }
    
   
    
    @IBAction func unwind( _ seg: UIStoryboardSegue) {
        
    }
           override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
               if UIDevice.current.userInterfaceIdiom == .phone {
                   return .allButUpsideDown
               } else {
                   return .all
               }
           }

           override var prefersStatusBarHidden: Bool {
               return true
           }
           override func viewDidDisappear(_ animated: Bool) {
               super.viewWillDisappear(animated)
               gameTimer?.invalidate()
           }
           @objc func appMovedToBackground() {
               stateCounter = 0
               registerBackgroundTask()
               // print("Welcome to the background world. . .")
               //sendNotification()
           }
           
           func registerBackgroundTask() {
               backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
                   self?.endBackgroundTask()
               }
               assert(backgroundTask != .invalid)
               
           }
           
           func endBackgroundTask() {
               // print("Background task ended.")
               UIApplication.shared.endBackgroundTask(backgroundTask)
               backgroundTask = .invalid

           }
    //ini putu
    
    var alert: UIAlertController!
           func showAlert() {
               alert = UIAlertController(title: "HEY! Are you sure you want to stop charging?", message: "You will crack your Egg prematurely! Quick - plug it back in to save your battery and Egg! \n Time Remaining \(countdown) second(s)", preferredStyle: .alert)
               //alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
               present(alert, animated: true){
                   Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
                       if self.countdown > 0 {
                           self.countdown = self.countdown - 1
                           // print(self.countdown)
                           if UIDevice.current.batteryState.rawValue == 2{
                               self.alert.dismiss(animated: true, completion: nil)
                               self.eggStatus = true
                           }
                       }
                    self.alert.message = "You will crack your Egg prematurely! Quick - plug it back in to save your battery and Egg! \n Time Remaining \(self.countdown) second(s)"
                       if self.countdown == 0 {
                       
                           self.alert.dismiss(animated: true, completion: nil)
                       }
                   }
               }
               
               
               //let when = DispatchTime.now() + 30
               //DispatchQueue.main.asyncAfter(deadline: when){
                   // your code with delay
                   self.eggStatus = false
                  // self.alert.dismiss(animated: true, completion: nil)
               //}
           }
           
           func sendNotification() {
               let notificationContent = UNMutableNotificationContent()
               notificationContent.title = "Whoa-whoa! Using your phone while charging?"
               notificationContent.body = "Remember, it will surely heat up your battery! You will crack your Egg prematurely! Hands-off!"
               notificationContent.badge = NSNumber(value: 1)
               
               if let url = Bundle.main.url(forResource: "dune",
                                           withExtension: "png") {
                   if let attachment = try? UNNotificationAttachment(identifier: "dune",
                                                                   url: url,
                                                                   options: nil) {
                       notificationContent.attachments = [attachment]
                   }
               }
               
               let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1,
                                                               repeats: false)
               let request = UNNotificationRequest(identifier: "testNotification",
                                                   content: notificationContent,
                                                   trigger: trigger)
               
               userNotificationCenter.add(request) { (error) in
                   if let error = error {
                       // print("Notification Error: ", error)
                   }
               }
               
           }
    override func viewWillAppear(_ animated: Bool) {
        self.currentGame?.restartGame()
        self.currentGame?.buildChar()
        self.currentGame?.buildChar2()
    }
    
    }
extension ViewController: GKGameCenterControllerDelegate {
    func gameCenterViewControllerDidFinish(_ gameCenterViewController:  GKGameCenterViewController) {
    gameCenterViewController.dismiss(animated: true, completion: nil)
    }
}




