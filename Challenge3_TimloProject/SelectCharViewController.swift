//
//  SelectCharViewController.swift
//  Challenge3_TimloProject
//
//  Created by Timotius Tjahjadi  on 28/05/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class SelectCharViewController: UIViewController {

    @IBOutlet weak var animalCollectionView: UICollectionView!
    @IBOutlet weak var animalNameLabel: UILabel!
    @IBOutlet weak var animalRarityLabel: UILabel!
    @IBOutlet weak var TotalLabel: UILabel!
    @IBOutlet weak var SelectButton: UIButton!
    
//    @IBAction func ExitButtonAction(_ sender: Any) {
//        self.navigationController?.popViewController(animated: false)
//    }
    
    @IBAction func SelectButtonAction(_ sender: Any) {
        UserDefaultData.saveSelectedAnimal(selectedAnimal: selectedAnimal)
        
        //MARK: This is for pick the data
        //animals[UserDefaultData.checkSelectedAnimal()].name
    }
    
    var animals = Animal.fetchAnimals()
    var selectedAnimal = 0
    
    let cellScale: CGFloat = 0.6

        override func viewDidLoad() {
            super.viewDidLoad()
            
            let screenSize = UIScreen.main.bounds.size
            let cellWidth = floor(screenSize.width * cellScale)
            let cellHeight = CGFloat(300.0) //floor(screenSize.height * cellScale)
            let insetX = (view.bounds.width - cellWidth) / 2.0
//            let insetY = (view.bounds.height - cellHeight) / 2.0
            
            let layout = animalCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
            
            layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
            animalCollectionView.contentInset = UIEdgeInsets(top: 0, left: insetX, bottom: 0, right: insetX)
            
            //MARK: Calculated the total of animal unlocked
            var totalAnimalUnlocked = 0
            
            for animal in animals {
                if(animal.unlocked == true){
                    totalAnimalUnlocked+=1
                }
            }
//            print(totalAnimalUnlocked)
            //
            
            //MARK: Change the BG into Gradient Color
            let gradientLayer = CAGradientLayer()
            
            gradientLayer.frame = self.view.bounds
            gradientLayer.colors = [UIColor.init(red: 0.714, green: 0.82, blue: 0.82, alpha: 1).cgColor, UIColor.init(red: 0.74, green: 0.83, blue: 0.52, alpha: 1).cgColor]
            self.view.layer.insertSublayer(gradientLayer, at: 0)
            //
            
            //MARK: Put outline to the Animal Label
            let strokeTextAttributes = [
              NSAttributedString.Key.strokeColor : UIColor.white,
              NSAttributedString.Key.foregroundColor : UIColor.systemOrange,
              NSAttributedString.Key.strokeWidth : -4.0,
              NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 40)]
              as [NSAttributedString.Key : Any]
            
            animalNameLabel.attributedText = NSMutableAttributedString(string: "Animals Name", attributes: strokeTextAttributes)
            //
            
            animalNameLabel.text = "Giraffe"
            animalRarityLabel.text = "common"
            TotalLabel.text = "\(totalAnimalUnlocked)/\(animals.count)"
            
            
            //MARK: Change the font and size
            TotalLabel.font = UIFont(name: "BowlbyOneSC-Regular",size:20)
            TotalLabel.adjustsFontForContentSizeCategory = true
            
            animalNameLabel.font = UIFont(name: "BowlbyOneSC-Regular",size:40)
            animalNameLabel.textColor = UIColor(red: 0.9, green: 0.54, blue: 0.34, alpha: 1)
            animalNameLabel.adjustsFontForContentSizeCategory = true
            
            animalRarityLabel.font = UIFont(name: "BowlbyOneSC-Regular",size:17)
            animalRarityLabel.adjustsFontForContentSizeCategory = true
            //
            
            animalCollectionView.dataSource = self
            animalCollectionView.delegate = self
        }

    }

    extension SelectCharViewController: UICollectionViewDataSource{
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return animals.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CharacterAnimal", for: indexPath) as! CharCollectionViewCell
            
            let animal = animals[indexPath.item]
            
            cell.animal = animal
        
            return cell
            
        }
        
    }

    extension SelectCharViewController: UIScrollViewDelegate, UICollectionViewDelegate{
        func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
            let layout = self.animalCollectionView?.collectionViewLayout as! UICollectionViewFlowLayout
            
            let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
            
            var offset = targetContentOffset.pointee
            let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
            let roundedIndex = round(index)

            offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: scrollView.contentInset.top)
            
            let animalAt = Int(roundedIndex)
            
            //MARK: Animal name and rarity Output
            if animals[animalAt].unlocked == false {
                animalNameLabel.text = "???"
                animalRarityLabel.text = "???"
                SelectButton.isEnabled = false
            }
            else{
                animalNameLabel.text = animals[animalAt].name
                animalRarityLabel.text = animals[animalAt].rarity
                SelectButton.isEnabled = true
                selectedAnimal = animalAt
            }
            //
            
            targetContentOffset.pointee = offset
        }
    }
