//
//  TodayViewController.swift
//  ChargeIT_Widget
//
//  Created by aldo on 22/06/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding, UIGestureRecognizerDelegate {
        
    let shapeLayer = CAShapeLayer()
    var battState : String = ""
    @IBOutlet weak var heavyUserBackground: UIImageView!
    @IBOutlet weak var lightUserBackground: UIImageView!
    @IBOutlet weak var heavyHours: UILabel!
    @IBOutlet weak var lightHours: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var batteryInfoLabel: UILabel!
    @IBOutlet weak var infoHeavyUsage: UILabel!
    @IBOutlet weak var infoLightUsage: UILabel!
    @IBOutlet weak var chargingLabel: UILabel!
    @IBOutlet weak var eggHatchInfo: UILabel!
    @IBOutlet weak var percentageLabel2: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.isBatteryMonitoringEnabled = true
        // Do any additional setup after loading the view.
        
        heavyUserBackground.layer.masksToBounds = false
        heavyUserBackground.layer.cornerRadius = 10
        heavyUserBackground.clipsToBounds = true
        
        
        lightUserBackground.layer.masksToBounds = false
        lightUserBackground.layer.cornerRadius = 10
        lightUserBackground.clipsToBounds = true
        
        
//        heavyHours.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        switch UIDevice.current.batteryState.rawValue {
               case 0:
                   self.battState = "Unknown"
               case 1:
                   self.battState = "UnPlugged"
               case 2:
                   self.battState = "Charging"
               case 3:
                   self.battState = "Full"
               default:
                   break
               }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        let sharedDefaults = UserDefaults.init(suiteName: "group.com.chargeit.widget")
        let text = sharedDefaults?.value(forKey: "playKey") as! String
        if (text == "Charging" && UIDevice.current.batteryState == .charging){
            print(text)
            heavyUserBackground.isHidden = true
            heavyHours.isHidden = true
            infoHeavyUsage.isHidden = true
            lightUserBackground.isHidden = true
            lightHours.isHidden = true
            infoLightUsage.isHidden = true
            batteryInfoLabel.isHidden = true
            percentageLabel.isHidden = true
            chargingLabel.isHidden = false
            eggHatchInfo.isHidden = false
            percentageLabel2.isHidden = false
            percentageLabel2.text = String(Int(CGFloat(UIDevice.current.batteryLevel*100)))+"%"
            shapeLayer.strokeColor = #colorLiteral(red: 0.368627451, green: 0.6862745098, blue: 0.4784313725, alpha: 1)
        }
    }
        
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        print(CGFloat(UIDevice.current.batteryLevel*100))
        
        
        
        let battery = CGFloat(UIDevice.current.batteryLevel*100)
        let center = CGPoint(x: 320, y: 50)
        let trackLayer = CAShapeLayer()
        let trackPath = UIBezierPath(arcCenter: center, radius: 45, startAngle: -CGFloat.pi / 2, endAngle: CGFloat.pi * 2, clockwise: true)
        trackLayer.path = trackPath.cgPath
        
        trackLayer.strokeColor = UIColor.clear.cgColor
        trackLayer.lineWidth = 10
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = CAShapeLayerLineCap.round
        view.layer.addSublayer(trackLayer)
        
        let circularPath = UIBezierPath(arcCenter: center, radius: 45, startAngle: -CGFloat.pi / 2, endAngle: (((CGFloat.pi/50.0) * battery) - CGFloat.pi / 2), clockwise: true)
        shapeLayer.path = circularPath.cgPath
        print(CGFloat.pi / 50)
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 10
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = CAShapeLayerLineCap.round
        
        shapeLayer.strokeEnd = 0
        
        view.layer.addSublayer(shapeLayer)
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")

        basicAnimation.toValue = 1
        
        basicAnimation.duration = 1
        
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        basicAnimation.isRemovedOnCompletion = false
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
        
        
        let batteryAfterInterval = Int(CGFloat(UIDevice.current.batteryLevel*100))

        // calculate the difference in battery levels over 1 minute

        let remainingPercentageHeavy = batteryAfterInterval * 17
        let remainingPercentageLight = batteryAfterInterval * 65

        let remainingTimeHeavy = remainingPercentageHeavy / 100
        let remainingTimeLight = remainingPercentageLight / 100
        print()
        print(batteryAfterInterval)
        print(remainingTimeHeavy)
        print(remainingPercentageHeavy)
        
        heavyHours.text = String(remainingTimeHeavy)+"hr"
        lightHours.text = String(remainingTimeLight)+"hr"
        percentageLabel.text = String(Int(CGFloat(UIDevice.current.batteryLevel*100)))+"%"
        
        if(battery <= 20 ){
        batteryInfoLabel.text = "Ready to charge"
        batteryInfoLabel.textColor = #colorLiteral(red: 0.9529411765, green: 0.7490196078, blue: 0.2823529412, alpha: 1)
        shapeLayer.strokeColor = #colorLiteral(red: 0.9529411765, green: 0.7490196078, blue: 0.2823529412, alpha: 1)
    let gesture = UITapGestureRecognizer(target: self, action: #selector(clickView(_:)))

    gesture.delegate = self
    view.addGestureRecognizer(gesture)
                      }
        
        completionHandler(NCUpdateResult.newData)
    }
    @objc func clickView(_ sender: UIView) {
       let url: URL? = URL(string: "chargeitTimlo://"  )

        if let appurl = url {
            self.extensionContext!.open(appurl,
                completionHandler: nil)
        }
    }
    
}
